FLAGS :=  -g -Wall -Wextra
LIB1 := -lX11
LIB2 := -lSDL2 -lSDL2_ttf

all: lc.bin

lc.bin: lc.o
	gcc -o lc lc.c $(FLAGS)

lc.o: lc.c
	gcc -c lc.c 

clean:
	rm -f *.o *~
