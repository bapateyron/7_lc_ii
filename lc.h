#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG
#ifdef DEBUG
   #define DUMPV(X) printf(#X"\n")
   #define DUMPD(X) printf(#X ": %d\n", X)
   #define DUMPC(X) printf(#X ": %c\n", X)
   #define DUMPS(X) printf(#X ": %s\n", X)
#else
   #define DUMPV(X)
   #define DUMPD(X)
   #define DUMPC(X)
   #define DUMPS(X)
#endif

#define ALLOUER(PTR, TYPE, NOMBRE)\
   if((PTR = (TYPE*)malloc(sizeof(TYPE)*NOMBRE))\
	 == NULL)\
	 puts("Echec d'allocation")


#define TAILLE_LIGNE 255
typedef struct cellule
{
   char		     ligne[TAILLE_LIGNE];
   struct cellule *  suiv;
} cellule_t;

typedef struct liste
{
   cellule_t * tete;
   cellule_t * fin;
} liste_t;

// Creer une cellule
cellule_t   *  creerCellule();

// Creer une liste
liste_t	    *  creerListe();

// Lire des chaines
void	       lireChaines(liste_t * l);

// Insertion en Tete
void	       insererTete(liste_t * l, char * s);

// Insertion en Fin
void	       insererFin(liste_t * l, char * s);
