#include "lc.h"

int main()
{
   char hi[] = "Hello world";
   DUMPS(hi);

   liste_t * l = creerListe();

   lireChaines(l); 


   // TODO freeListe();
   free(l);

   return 0;
}


liste_t * creerListe()
{
   liste_t * l = NULL;
   ALLOUER(l, liste_t, 1);

   // TODO Ajouter une première cellule !!!!
   // Liste est un POINTEUR DE Liste!
   return l;
}


void lireChaines(liste_t * l)
{
   char buffer[TAILLE_LIGNE] = {0};

   while (strcmp(buffer, "EOF"))
   {
      fgets(buffer, TAILLE_LIGNE, stdin);
      insererFin(l, buffer);
   }
}


cellule_t * creerCellule()
{
   // Allocation
   cellule_t * c  = NULL;
   ALLOUER(c, cellule_t, 1); 

   // Initialisation
   for(int i = 0; i < TAILLE_LIGNE; i++) c->ligne[i] = 0;
   c->suiv = NULL;

   return c;
}

void insererTete(liste_t * l, char * s)
{
   cellule_t * c  = creerCellule();

   c->suiv	  = l->tete;
   l->tete	  = c;

   strcpy(c->ligne, s);
}

void insererFin(liste_t * l, char * s)
{
   cellule_t * c  = creerCellule();

   c->suiv	  = NULL;
   l->fin->suiv	  = c;
   l->fin	  = c;

   strcpy(c->ligne, s);
}
